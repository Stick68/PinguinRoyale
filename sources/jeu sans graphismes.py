import random
from copy import deepcopy

print('menu')   #Pour l'affichage textuel

barragevie = 12 #Initialise une variable liée à l'objet barrage

nbactionsinit = 2   #Initalise la variable qui définit le nombre d'actions que comporte un tour

def GetObj():   #Fonction qui renvoit un texte correspondant à l'objet obtenu

    a = random.randint(0,70) # Sert à donner une valeur à a pour définir aléatoirement quel effet sera appliqué

    if 0 <= a < 10:
        return 'dynamite'
    if 10 <= a < 20:
        return 'verglas'
    if 20 <= a < 30:
        return 'montagne'
    if 30 <= a < 40:
        return 'curl'
    if 40 <= a < 50:
        return 'ski'
    if 50 <= a < 60:
        return 'barrage'
    if 60 <= a < 70:
        return 'transformation'

def Spawn():    #Fonction renvoyant les coordonées x et y d'une case 'spawnable' pour un joueur
    e = 0
    while e < 1:
        y = random.randint(0,len(map)-1)
        x = random.randint(0,len(map[0])-1)
        if GetMapFree(x,y)==True:
            e = 1
            return x, y

def SpawnAction():  #Fonction renvoyant un case 'spawnable' pour une nouvelle case, excepté une nouvelle case verglas
    e = 0
    while e < 1:
        y = random.randint(0,len(map)-1)
        x = random.randint(0,len(map[0])-1)
        if GetMap4(x,y)=='X'or GetMap4(x,y)=='V':
            e = 1
            return x, y

def SpawnActionVerglas():
    e = 0
    while e < 1:
        y = random.randint(0,len(map)-1)
        x = random.randint(0,len(map[0])-1)
        if GetMap4(x,y)=='X':
            e = 1
            return x, y

def SpawnObj():
    e = 0
    while e < 1:
        y = random.randint(0,len(map)-1)
        x = random.randint(0,len(map[0])-1)
        if GetMap(x,y)=='X':
            e = 1
            return x, y

def Mouvement(direction, cible):
    portee = 1

    if direction == 'z':

        while True:
            case = GetMap(cible[0],cible[1]-portee)
            if case == 'M' or case == 'P1' or case == 'P2' or case =='P3' or case == 'P4' or case == 'B':
                return cible[0], cible[1]-portee+1
            elif case == 'V':
                portee += 1
            else:
                return cible[0],(cible[1]-portee)
    if direction == 's':
        while True:
            case = GetMap(cible[0],cible[1]+portee)
            if case == 'M' or case == 'P1' or case == 'P2' or case =='P3' or case == 'P4' or case == 'B':
                return cible[0], cible[1]+portee-1
            elif case == 'V':
                portee += 1
            else:
                return cible[0],(cible[1]+portee)

    if direction == 'q':

        while True:
            case = GetMap(cible[0]-portee,cible[1])
            if case == 'M' or case == 'P1' or case == 'P2' or case =='P3' or case == 'P4' or case == 'B':
                return cible[0]-portee+1, cible[1]
            elif case == 'V':
                portee += 1
            else:
                return (cible[0]-portee),cible[1]
    if direction == 'd':
        while True:
            case = GetMap(cible[0]+portee,cible[1])
            if case == 'M' or case == 'P1' or case == 'P2' or case =='P3' or case == 'P4' or case == 'B':
                return cible[0]+portee-1, cible[1]
            elif case == 'V':
                portee += 1
            else:
                return (cible[0]+portee),cible[1]



    else:
            print('faux')

def Affichage(x):
    for i in x:
        print(i)


def GetMapFree(x,y):
    if map[y][x]=='X':
        return True
    return False

def GetMap(x,y):
    return map2[y][x]

def GetMap4(x,y):
    return map4[y][x]



try:
    menu_avancement = 0

    while menu_avancement < 1:
        nb_joueurs = int(input('entrez le nombre de joueurs'))
        assert nb_joueurs== 2 or nb_joueurs== 3 or nb_joueurs== 4
        if nb_joueurs == 2:
            joueurs = {'P1':[],'P2':[]}
            inventaires = {'P1':[],'P2':[]}
        elif nb_joueurs == 3:
            joueurs = {'P1':[],'P2':[],'P3':[]}
            inventaires = {'P1':[],'P2':[],'P3':[]}
        else:
            joueurs = {'P1':[],'P2':[],'P3':[],'P4':[]}
            inventaires = {'P1':[],'P2':[],'P3':[],'P4':[]}

        print('il y a', nb_joueurs, 'joueurs')
        objets = []
        barrage = []
        menu_avancement = 1

    while 0 < menu_avancement < 2:
        print('Choisissez parmi les cartes suivantes :')
        print('1 : normal')
        print('2 : verglas')
        print('3 : montagnes')
        choix_map = int(input('entrez le numéro correspondant'))

        assert choix_map == 1 or choix_map == 2 or choix_map == 3

        print('vous avez choisi la map numéro', choix_map)
        if choix_map==1:
            map = [['T','T','T','T','T','T','T'],['T','X','X','X','X','X','T'],['T','X','X','X','X','X','T'],['T','X','X','X','X','X','T'],['T','X','X','X','X','X','T'],['T','X','X','X','X','X','T'],['T','T','T','T','T','T','T']]
        if choix_map==2:
            map = [['T','T','T','T','T','T','T'],['T','X','X','M','M','M','T'],['T','V','V','V','V','V','T'],['T','V','V','V','V','V','T'],['T','V','V','V','V','V','T'],['T','X','X','X','X','X','T'],['T','T','T','T','T','T','T']]
        if choix_map==3:
            map = [['T','T','T','T','T','T','T'],['T','M','M','M','M','M','T'],['T','M','M','M','M','M','T'],['T','M','M','M','M','M','T'],['T','M','M','M','M','M','T'],['T','X','X','X','X','X','T'],['T','T','T','T','T','T','T']]
        menu_avancement = 2

    while 1 <menu_avancement < 3:
        print('jeu')

        for a in joueurs.keys():
            succes = 0
            while succes == 0:
                spawn = Spawn()[0],Spawn()[1]
                if spawn not in joueurs.values():
                    joueurs[a]= spawn
                    succes = 1

        menu_avancement = 3

    while 2 < menu_avancement < 4:

        tour = 1

        while nb_joueurs>1:

            for a in joueurs.keys():



                if joueurs[a] != 'mort':

                    print('à',a,'de jouer !')
                    nb_actions = nbactionsinit

                    while nb_actions > 0:


                            map2 = deepcopy(map)
                            for b in joueurs.keys():
                                if joueurs[b]!='mort':
                                    if map[joueurs[b][1]][joueurs[b][0]]=='T':
                                        print('mort')
                                        joueurs[b]='mort'
                                        nb_joueurs -=1
                                    else:
                                        map2[joueurs[b][1]][joueurs[b][0]]=b

                            for b in barrage:
                                if b[1]>1:
                                    map2[b[0][1]][b[0][0]]='B'
                                    b[1] -= 1
                                else:
                                    barrage.remove(b)

                            map4 = deepcopy(map2)

                            for b in objets:
                                if map4[b[1]][b[0]]== 'X':
                                    map4[b[1]][b[0]]='O'
                                else:
                                    objets.remove(b)



                            Affichage(map4)

                            if nb_joueurs==1:
                                for b in joueurs:
                                    if joueurs[b]!="mort":
                                        print('joueur', b,'a gagné')
                                        menu_avancement = 0

                            if joueurs[a]!= 'mort':
                                print('Inventaire:', inventaires[a])
                                action = str(input('faire:'))
                                if action == 'z' or action == 's' or action == 'q' or action == 'd':
                                    joueurs[a] = Mouvement(action, joueurs[a])
                                    for c in objets:
                                        if c == joueurs[a]:
                                            objets.remove(c)
                                            inventaires[a] = GetObj()
                                if action == 'o':
                                    if inventaires[a]=='dynamite':
                                        xy = SpawnAction()
                                        map[xy[1]][xy[0]] = 'T'
                                        inventaires[a]=[]
                                    if inventaires[a]=='verglas':
                                        xy = SpawnActionVerglas()
                                        map[xy[1]][xy[0]] = 'V'
                                        inventaires[a]=[]
                                    if inventaires[a]=='montagne':
                                        xy = SpawnAction()
                                        map[xy[1]][xy[0]] = 'M'
                                        inventaires[a]=[]
                                    if inventaires[a]=='ski':
                                        nb_actions += 2
                                        inventaires[a]=[]
                                    if inventaires[a]=='curl':
                                        direc = str(input('direction :'))
                                        if direc == 'z' or direc == 's' or direc == 'q' or direc == 'd':
                                            if direc == 'z':
                                                posx = joueurs[a][0]
                                                posy = joueurs[a][1] - 1
                                                while posy > 0:
                                                    if map2[posy][posx]=='P1' or map2[posy][posx]=='P2' or map2[posy][posx]=='P3' or map2[posy][posx]=='P4':
                                                        target = map2[posy][posx]
                                                        joueurs[target] = Mouvement(direc, joueurs[target])
                                                        if map[joueurs[target][1]][joueurs[target][0]] == 'T':
                                                            joueurs[target] = 'mort'
                                                        else:
                                                            joueurs[target] = Mouvement(direc, joueurs[target])
                                                        posy = 0
                                                    elif map2[posy][posx]=='X' or map2[posy][posx]=='V':
                                                        posy -= 1
                                                    else:
                                                        posy = 0
                                            if direc == 's':
                                                posx = joueurs[a][0]
                                                posy = joueurs[a][1] + 1
                                                while posy < len(map):
                                                    if map2[posy][posx]=='P1' or map2[posy][posx]=='P2' or map2[posy][posx]=='P3' or map2[posy][posx]=='P4':
                                                        target = map2[posy][posx]
                                                        joueurs[target] = Mouvement(direc, joueurs[target])
                                                        if map[joueurs[target][1]][joueurs[target][0]] == 'T':
                                                            joueurs[target] = 'mort'
                                                        else:
                                                            joueurs[target] = Mouvement(direc, joueurs[target])
                                                        posy = len(map)
                                                    elif map2[posy][posx]=='X' or map2[posy][posx]=='V':
                                                        posy += 1
                                                    else:
                                                        posy = len(map)
                                            if direc == 'q':
                                                posx = joueurs[a][0] - 1
                                                posy = joueurs[a][1]
                                                while posx  > 0:
                                                    if map2[posy][posx]=='P1' or map2[posy][posx]=='P2' or map2[posy][posx]=='P3' or map2[posy][posx]=='P4':
                                                        target = map2[posy][posx]
                                                        joueurs[target] = Mouvement(direc, joueurs[target])
                                                        if map[joueurs[target][1]][joueurs[target][0]] == 'T':
                                                            joueurs[target] = 'mort'
                                                        else:
                                                            joueurs[target] = Mouvement(direc, joueurs[target])
                                                        posx = 0
                                                    elif map2[posy][posx]=='X' or map2[posy][posx]=='V':
                                                        posx -= 1
                                                    else:
                                                        posx = 0
                                            if direc == 'd':
                                                posx = joueurs[a][0] + 1
                                                posy = joueurs[a][1]
                                                while posx < len(map):
                                                    if map2[posy][posx]=='P1' or map2[posy][posx]=='P2' or map2[posy][posx]=='P3' or map2[posy][posx]=='P4':
                                                        target = map2[posy][posx]
                                                        joueurs[target] = Mouvement(direc, joueurs[target])
                                                        if map[joueurs[target][1]][joueurs[target][0]] == 'T':
                                                            joueurs[target] = 'mort'
                                                        else:
                                                            joueurs[target] = Mouvement(direc, joueurs[target])
                                                        posx = len(map)
                                                    elif map2[posy][posx]=='X' or map2[posy][posx]=='V':
                                                        posx += 1
                                                    else:
                                                        posx = len(map)

                                            inventaires[a]=[]
                                    if inventaires[a]=='barrage':
                                        direc = str(input('direction :'))
                                        if direc == 'z' or direc == 's' or direc == 'q' or direc == 'd' or direc == 'a':
                                            e = 0
                                            while e < 1:
                                                if direc == 'z':
                                                    posx = joueurs[a][0]
                                                    posy = joueurs[a][1] - 1
                                                    if GetMap4(posx,posy)=='X' or GetMap4(posx,posy)=='V':
                                                        barrage.append([(posx,posy),barragevie])
                                                        map2[posy][posx]='B'
                                                        inventaires[a]=[]
                                                        e+=1


                                                if direc == 's':
                                                    posx = joueurs[a][0]
                                                    posy = joueurs[a][1] + 1
                                                    if GetMap4(posx,posy)=='X' or GetMap4(posx,posy)=='V':
                                                        barrage.append([(posx,posy),barragevie])
                                                        map2[posy][posx]='B'
                                                        inventaires[a]=[]
                                                        e+=1

                                                if direc == 'q':
                                                    posx = joueurs[a][0] - 1
                                                    posy = joueurs[a][1]
                                                    if GetMap4(posx,posy)=='X' or GetMap4(posx,posy)=='V':
                                                        barrage.append([(posx,posy),barragevie])
                                                        map2[posy][posx]='B'
                                                        inventaires[a]=[]
                                                        e+=1
                                                if direc == 'd':
                                                    posx = joueurs[a][0] + 1
                                                    posy = joueurs[a][1]
                                                    if GetMap4(posx,posy)=='X' or GetMap4(posx,posy)=='V':
                                                        barrage.append([(posx,posy),barragevie])
                                                        map2[posy][posx]='B'
                                                        inventaires[a]=[]
                                                        e+=1
                                                if direc == 'a':
                                                        nb_actions+=1
                                                        e+=1
                                    if inventaires[a]=='transformation':
                                        direc = str(input('direction :'))
                                        if direc == 'z' or direc == 's' or direc == 'q' or direc == 'd' or direc == 'a':
                                            e = 0
                                            while e < 1:
                                                if direc == 'z':
                                                    posx = joueurs[a][0]
                                                    posy = joueurs[a][1] - 1
                                                    if (GetMap4(posx,posy)=='M' or GetMap4(posx,posy)=='V' or GetMap4(posx,posy)=='T') and 0 < posy < (len(map)-1) and 0 < posx < (len(map[0])-1):
                                                        map[posy][posx]='X'
                                                        inventaires[a]=[]
                                                        e+=1
                                                    else:
                                                        nb_actions+=1
                                                        e+=1


                                                if direc == 's':
                                                    posx = joueurs[a][0]
                                                    posy = joueurs[a][1] + 1
                                                    if (GetMap4(posx,posy)=='M' or GetMap4(posx,posy)=='V' or GetMap4(posx,posy)=='T') and 0 < posy < (len(map)-1) and 0 < posx < (len(map[0])-1):
                                                        map[posy][posx]='X'
                                                        inventaires[a]=[]
                                                        e+=1
                                                    else:
                                                        nb_actions+=1
                                                        e+=1

                                                if direc == 'q':
                                                    posx = joueurs[a][0] - 1
                                                    posy = joueurs[a][1]
                                                    if (GetMap4(posx,posy)=='M' or GetMap4(posx,posy)=='V' or GetMap4(posx,posy)=='T') and 0 < posy < (len(map)-1) and 0 < posx < (len(map[0])-1):
                                                        map[posy][posx]='X'
                                                        inventaires[a]=[]
                                                        e+=1
                                                    else:
                                                        nb_actions+=1
                                                        e+=1
                                                if direc == 'd':
                                                    posx = joueurs[a][0] + 1
                                                    posy = joueurs[a][1]
                                                    if (GetMap4(posx,posy)=='M' or GetMap4(posx,posy)=='V' or GetMap4(posx,posy)=='T') and 0 < posy < (len(map)-1) and 0 < posx < (len(map[0])-1):
                                                        map[posy][posx]='X'
                                                        inventaires[a]=[]
                                                        e+=1
                                                    else:
                                                        nb_actions+=1
                                                        e+=1
                                                if direc == 'a':
                                                        nb_actions+=1
                                                        e+=1



                                nb_actions -= 1
                            else:
                                nb_actions=0


                    if tour%2==1 or random.randint(0,1)==1:
                        objets.append(SpawnObj())


                tour += 1
                nb_actions = nbactionsinit





except:
    print('erreur')
