import tkinter as tk
from tkinter import messagebox
from PIL import Image, ImageTk
import random

class MenuGraphique(tk.Tk):

    def __init__(self):
        super().__init__()
        self.tour=0
        self.compteur_cadeaux = 0
        self.noms_objets = {
            'O1': 'barrage',
            'O2': 'montagne',
            'O3': 'verglas',
            'O4': 'dynamite',
            'O5': 'ski',
            'O6': 'curling',
            'O7': 'transformation'
        }
        self.cadeaux_deja_trouves = []
        self.geometry("800x600")
        self.resizable(True, True)
        self.nb_joueurs = None
        self.delai_reapparition = 30

        self.choix_map = None
        self.joueurs = []
        self.cadeau_initial_trouve = False
        self.cadeaux_mysteres = []
        self.objet_ramasse = False
        self.cadeau_present = False
        self.demarrer_partie()



        logo_image = Image.open(r"images/pingouin.png")
        logo_photo = ImageTk.PhotoImage(logo_image)
        self.title("Pingouin Royal")

        self.images_cases = {
            'X': Image.open(r"images/case2.png"),
            'M': Image.open(r"images/montagnes.png"),
            'V': Image.open(r"images/verglas.png"),
            'B': Image.open(r"images/barrage.png"),

        }

        self.images_cadeaux = {
            'Cadeau': Image.open(r"images/cadeau.png")
        }

        self.images_objets = {
            'O1': Image.open(r"images/barrage2.png"),
            'O2': Image.open(r"images/montagnes2.png"),
            'O3': Image.open(r"images/verglas2.png"),
            'O4': Image.open(r"images/dynamite.png"),
            'O5': Image.open(r"images/skis.png"),
            'O6': Image.open(r"images/curling.png"),
            'O7': Image.open(r"images/transformation.png")
        }

        self.images_joueurs = {
            'P1': Image.open(r"images/P1.png"),
            'P2': Image.open(r"images/P2.png"),
            'P3': Image.open(r"images/P3.png"),
            'P4': Image.open(r"images/P4.png"),
        }

        self.label_nb_joueurs = tk.Label(self, text="Combien de joueurs ?")
        self.label_nb_joueurs.pack()

        self.entry_nb_joueurs = tk.Entry(self)
        self.entry_nb_joueurs.pack()

        self.bouton_nb_joueurs = tk.Button(self, text="Valider", command=self.valider_nb_joueurs)
        self.bouton_nb_joueurs.pack()

        # Variable de classe pour stocker les objets disponibles
        self.objets_disponibles = list(self.images_objets.keys())

    def demarrer_partie(self):
        # Initialisation de la partie, affichage du plateau, etc.
        self.mettre_a_jour_jeu()

    def valider_nb_joueurs(self):
        nb_joueurs = self.entry_nb_joueurs.get()
        if not nb_joueurs.isdigit():
            messagebox.showerror("Erreur", "Veuillez saisir un nombre entier.")
            return
        nb_joueurs = int(nb_joueurs)
        if nb_joueurs < 2 or nb_joueurs > 4:
            messagebox.showerror("Erreur", "Le nombre de joueurs doit être compris entre 2 et 4.")
            return

        self.nb_joueurs = nb_joueurs

        self.label_choix_carte = tk.Label(self, text="Quelle carte souhaitez-vous choisir ?")
        self.label_choix_carte.pack()

        self.bouton_map_1 = tk.Button(self, text="Play ! ", command=lambda: self.choisir_map(2))
        self.bouton_map_1.pack()

        self.label_nb_joueurs.destroy()
        self.entry_nb_joueurs.destroy()
        self.bouton_nb_joueurs.destroy()

    def choisir_map(self, choix_map):
        self.choix_map = choix_map
        self.cadeaux_deja_trouves = []

        self.map = [['T', 'T', 'T', 'T', 'T', 'T', 'T'],
                    ['T', 'X', 'X', 'X', 'X', 'X', 'T'],
                    ['T', 'X', 'X', 'X', 'X', 'X', 'T'],
                    ['T', 'X', 'X', 'X', 'X', 'X', 'T'],
                    ['T', 'X', 'X', 'X', 'X', 'X', 'T'],
                    ['T', 'X', 'X', 'X', 'X', 'X', 'T'],
                    ['T', 'T', 'T', 'T', 'T', 'T', 'T']]

        # Initialisation de la liste des cadeaux mystères
        self.initialiser_cadeaux_mysteres()

        self.afficher_plateau()
        self.label_choix_carte.destroy()
        self.bouton_map_1.destroy()


    def initialiser_cadeaux_mysteres(self):
        self.positions_disponibles = [(i, j) for i, ligne in enumerate(self.map) for j, case in enumerate(ligne) if case == 'X']

        if self.positions_disponibles and not self.cadeaux_mysteres:
            position_cadeau = random.choice(self.positions_disponibles)
            self.cadeaux_mysteres.append(position_cadeau)

    def afficher_plateau(self):
        cadre_bleu = tk.Frame(self, bg="#4287f5", width=500, height=500)
        cadre_bleu.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        self.canvas_plateau = tk.Canvas(cadre_bleu, bg="#4287f5", width=500, height=500)
        self.canvas_plateau.pack(fill=tk.BOTH, expand=True)

        taille_case = 400
        spacing = 90

        self.images_displayed = []

        for i, ligne in enumerate(self.map):
            for j, case in enumerate(ligne):
                if case in self.images_cases:
                    resized_image = self.images_cases[case].resize((taille_case, taille_case))
                    photo = ImageTk.PhotoImage(resized_image)
                    self.images_displayed.append(photo)
                    self.canvas_plateau.create_image(j * spacing, i * spacing, anchor=tk.NW, image=photo)

        self.placer_cadeaux()  # Placer les cadeaux mystères

        cadre_blanc = tk.Frame(self, bg="white")
        cadre_blanc.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        label_blanc = tk.Label(cadre_blanc, text="rien ")
        label_blanc.pack(fill=tk.BOTH, expand=True)

        self.afficher_joueurs(taille_case, spacing)

        self.bind("<KeyPress-z>", lambda event: self.deplacer_joueur('haut'))
        self.bind("<KeyPress-q>",lambda event: self.deplacer_joueur('gauche'))
        self.bind("<KeyPress-s>", lambda event: self.deplacer_joueur('bas'))
        self.bind("<KeyPress-d>", lambda event: self.deplacer_joueur('droite'))

    def afficher_joueurs(self, taille_case, spacing):
        cases_autorisees = ['X']

        positions_disponibles = []

        for i, ligne in enumerate(self.map):
            for j, case in enumerate(ligne):
                if case in cases_autorisees:
                    positions_disponibles.append((i, j))

        if len(positions_disponibles) < self.nb_joueurs:
            messagebox.showerror("Erreur", "Il n'y a pas suffisamment de cases autorisées pour placer tous les joueurs.")
            return

        joueurs_positions = random.sample(positions_disponibles, self.nb_joueurs)

        for i, (ligne, colonne) in enumerate(joueurs_positions):
            joueur_image = self.images_joueurs[f'P{i+1}']
            resized_image = joueur_image.resize((taille_case, taille_case))
            photo = ImageTk.PhotoImage(resized_image)
            self.images_displayed.append(photo)
            self.canvas_plateau.create_image(colonne * spacing, ligne * spacing, anchor=tk.NW, image=photo, tags=f"photo{i+1}")
            self.joueurs.append((i+1, ligne, colonne, photo))

        cadre_blanc = tk.Frame(self, bg="white")
        cadre_blanc.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)



        self.demarrer_jeu()

    def demarrer_jeu(self):
        self.joueur_actif = 1
        messagebox.showinfo("Début du jeu", f"Joueur {self.joueur_actif}, c'est à vous de jouer !")
        self.activer_controles()

    def deplacer_joueur(self, direction):
        # Vérifier si le joueur est mort
        joueur = self.joueurs[self.joueur_actif - 1]
        if joueur is None:
            return  # Quitter la fonction si le joueur est mort

        # Récupérer les coordonnées actuelles du joueur
        ligne, colonne = joueur[1], joueur[2]

        # Calculer les nouvelles coordonnées en fonction de la direction
        if direction == 'haut':
            nouvelle_ligne = ligne - 1
            nouvelle_colonne = colonne
        elif direction == 'bas':
            nouvelle_ligne = ligne + 1
            nouvelle_colonne = colonne
        elif direction == 'gauche':
            nouvelle_ligne = ligne
            nouvelle_colonne = colonne - 1
        elif direction == 'droite':
            nouvelle_ligne = ligne
            nouvelle_colonne = colonne + 1


                    # Empêcher les joueurs de sortir de la map
        if direction == 'haut' and ligne == 0:
            return
        if direction == 'bas' and ligne == len(self.map) - 1:
            return
        if direction == 'gauche' and colonne == 0:
            return
        if direction == 'droite' and colonne == len(self.map[0]) - 1:
            return

        """if case_occupee[ligne][colonne] is not None:
            return"""

        # Vérifier si la nouvelle position est valide
        if self.position_valide(nouvelle_ligne, nouvelle_colonne):
            for joueur in self.joueurs:
                if joueur is not None and (joueur[1], joueur[2]) == (nouvelle_ligne, nouvelle_colonne):
                    messagebox.showinfo("Déplacement invalide", "Vous ne pouvez pas vous déplacer sur une case occupée par un autre joueur.")
                    return # Quitter la fonction si la case est occupée par un autre joueur

            # Vérifier s'il y a un cadeau mystère sur la case de destination
            self.verifier_cadeau_mystere(nouvelle_ligne, nouvelle_colonne)

            # Déplacer visuellement le joueur sur le plateau
            deplacement_x = (nouvelle_colonne - colonne) * 90
            deplacement_y = (nouvelle_ligne - ligne) * 90
            self.canvas_plateau.move(f"photo{self.joueur_actif}", deplacement_x, deplacement_y)

            # Mettre à jour les informations du joueur seulement s'il est vivant
            if joueur is not None:
                self.joueurs[self.joueur_actif - 1] = (self.joueur_actif, nouvelle_ligne, nouvelle_colonne, joueur[3])

            # Vérifier si le joueur est arrivé sur une case d'eau ('T')
            if self.map[nouvelle_ligne][nouvelle_colonne] == 'T':
                self.joueur_meurt() # Appeler la méthode joueur_meurt() pour gérer la mort du joueur
            else:
            # Passer au joueur suivant si le joueur n'est pas mort
                self.passer_au_joueur_suivant()
                print(nouvelle_ligne, nouvelle_colonne)
        else:
            messagebox.showinfo("Erreur", "Déplacement invalide dans cette direction.")




    def joueur_meurt(self):
        # Marquer le joueur comme mort en supprimant son entrée dans la liste des joueurs
        joueur_mort = self.joueurs[self.joueur_actif - 1]
        self.joueurs[self.joueur_actif - 1] = None
        messagebox.showinfo("Joueur mort", f"Le joueur {self.joueur_actif} est mort.")
        self.canvas_plateau.delete(f"photo{self.joueur_actif}")
        # Passer au joueur suivant
        # Passer au joueur suivant
        self.passer_au_joueur_suivant()
        joueurs_en_vie = [joueur for joueur in self.joueurs if joueur is not None]
        if len(joueurs_en_vie) == 1:
            gagnant = [joueur for joueur in joueurs_en_vie if joueur is not None][0]
            messagebox.showinfo("Fin du jeu", f"Le joueur {gagnant[0]} est le gagnant !")
            self.quit()  # Arrêter le jeu une fois qu'il n'y a plus qu'un seul joueur en vie


    def placer_cadeaux(self):
        self.canvas_plateau.delete("cadeau")  # Supprimer tous les cadeaux mystères actuellement affichés sur le plateau

        for position in self.cadeaux_mysteres:
            ligne, colonne = position
            taille_case = 400
            spacing = 90
            x = colonne * spacing
            y = ligne * spacing
            cadeau_image = self.images_cadeaux['Cadeau']
            resized_image = cadeau_image.resize((taille_case, taille_case))
            photo = ImageTk.PhotoImage(resized_image)
            self.images_displayed.append(photo)
            self.canvas_plateau.create_image(x, y, anchor=tk.NW, image=photo, tags=f"cadeau_{ligne}_{colonne}")
            self.canvas_plateau.create_image(x, y, anchor=tk.NW, image=photo, tags=f"cadeau_{ligne}_{colonne}")



    def ramasser_cadeau_mystere(self, joueur):
        # Récupérer les coordonnées du joueur
        joueur_ligne, joueur_colonne = joueur[1], joueur[2]
        objet_aleatoire = random.choice(self.objets_disponibles)

        # Vérifier si l'objet ramassé est "verglas"
        if objet_aleatoire == 'O3':
            # Générer des coordonnées aléatoires pour la position du verglas
            V1 = random.randint(1, 5)
            V2 = random.randint(1, 5)
            # Assurez-vous que les coordonnées sont valides
            if self.position_valide(V1, V2):
                # Modifier la case correspondante en verglas
                self.map[V1][V2] = 'V'
                # Afficher visuellement le changement sur le plateau
                self.afficher_plateau()
                # Supprimer le cadeau mystère de la carte et de la liste des cadeaux mystères
                self.canvas_plateau.delete(f"cadeau_{joueur_ligne}_{joueur_colonne}")
                self.cadeaux_mysteres.remove((joueur_ligne, joueur_colonne))
                # Replacer les cadeaux mystères sur la carte
                self.placer_cadeaux()
                # Mettre à jour la variable indiquant la présence d'un cadeau mystère
                self.cadeau_present = False
        elif objet_aleatoire == 'O2':
            # Générer des coordonnées aléatoires pour la position du verglas
            V1 = random.randint(1, 5)
            V2 = random.randint(1, 5)
            # Assurez-vous que les coordonnées sont valides
            if self.position_valide(V1, V2):
                # Modifier la case correspondante en verglas
                self.map[V1][V2] = 'M'
                # Afficher visuellement le changement sur le plateau
                self.afficher_plateau()
                # Supprimer le cadeau mystère de la carte et de la liste des cadeaux mystères
                self.canvas_plateau.delete(f"cadeau_{joueur_ligne}_{joueur_colonne}")
                self.cadeaux_mysteres.remove((joueur_ligne, joueur_colonne))
                # Replacer les cadeaux mystères sur la carte
                self.placer_cadeaux()
                # Mettre à jour la variable indiquant la présence d'un cadeau mystère
                self.cadeau_present = False
        elif objet_aleatoire == 'O4':
            # Générer des coordonnées aléatoires pour la position du verglas
            V1 = random.randint(1, 5)
            V2 = random.randint(1, 5)
            # Assurez-vous que les coordonnées sont valides
            if self.position_valide(V1, V2):
                # Modifier la case correspondante en verglas
                self.map[V1][V2] = 'T'
                # Afficher visuellement le changement sur le plateau
                self.afficher_plateau()
                # Supprimer le cadeau mystère de la carte et de la liste des cadeaux mystères
                self.canvas_plateau.delete(f"cadeau_{joueur_ligne}_{joueur_colonne}")
                self.cadeaux_mysteres.remove((joueur_ligne, joueur_colonne))
                # Replacer les cadeaux mystères sur la carte
                self.placer_cadeaux()
                # Mettre à jour la variable indiquant la présence d'un cadeau mystère
                self.cadeau_present = False


            else:
                messagebox.showwarning("Coordonnées invalides", "Les coordonnées générées pour le verglas sont invalides.")
        else:
            messagebox.showinfo("Objet ramassé", f"Vous avez trouvé un {self.noms_objets[objet_aleatoire]} !")




    def activer_controles(self):
        super().activer_controles()
        self.bind("<KeyPress-z>", lambda event: self.deplacer_joueur('haut'))
        self.bind("<KeyPress-q>", lambda event: self.deplacer_joueur('gauche'))
        self.bind("<KeyPress-s>", lambda event: self.deplacer_joueur('bas'))
        self.bind("<KeyPress-d>", lambda event: self.deplacer_joueur('droite'))






    def supprimer_barrage(self, ligne, colonne):
        # Supprimer visuellement le barrage du plateau
        self.canvas_plateau.delete(f"barrage_{ligne}_{colonne}")
        # Supprimer le barrage de la carte
        self.map[ligne][colonne] = 'X'

    def placer_barrage(self):
        # Récupérer les coordonnées du joueur actif
        joueur = self.joueurs[self.joueur_actif - 1]
        ligne, colonne = joueur[1], joueur[2]

        # Placer le barrage sur une case adjacente à celle du joueur
        for dl, dc in [(0, -1), (0, 1), (-1, 0), (1, 0)]:  # Déplacements possibles (gauche, droite, haut, bas)
            nouvelle_ligne, nouvelle_colonne = ligne + dl, colonne + dc
            # Vérifier si la nouvelle position est valide et que la case est libre
            if self.position_valide(nouvelle_ligne, nouvelle_colonne) and self.map[nouvelle_ligne][nouvelle_colonne] == 'X':
                # Placer le barrage sur la case adjacente
                self.map[nouvelle_ligne][nouvelle_colonne] = 'B'
                # Afficher visuellement le barrage sur le plateau
                taille_case = 400
                spacing = 90
                barrage_image = self.images_objets['O1']
                resized_image = barrage_image.resize((taille_case, taille_case))
                photo = ImageTk.PhotoImage(resized_image)
                self.images_displayed.append(photo)
                self.canvas_plateau.create_image(nouvelle_colonne * spacing, nouvelle_ligne * spacing, anchor=tk.NW, image=photo, tags=f"barrage_{nouvelle_ligne}_{nouvelle_colonne}")
                # Mettre à jour visuellement le plateau
                self.canvas_plateau.update()
                # Lancer le compte à rebours pour supprimer le barrage après 12 tours
                self.after(12000, lambda: self.supprimer_barrage(nouvelle_ligne, nouvelle_colonne))
                break  # Arrêter la recherche après avoir placé le barrage sur la première case libre


    def verifier_cadeau_mystere(self, ligne, colonne):
        if (ligne, colonne) in self.cadeaux_mysteres and (ligne, colonne) not in self.cadeaux_deja_trouves:
            self.utiliser_cadeau_sur_case(ligne, colonne)
            self.cadeaux_deja_trouves.append((ligne, colonne))
            self.compteur_cadeaux = 0  # Réinitialiser le compteur

    def utiliser_cadeau_sur_case(self, ligne, colonne):
        if (ligne, colonne) in self.cadeaux_mysteres:
            if (ligne, colonne) not in self.cadeaux_deja_trouves:
                messagebox.showinfo("Cadeau mystère", "Vous avez trouvé un cadeau mystère !")
                self.cadeaux_deja_trouves.append((ligne, colonne))
                objet_aleatoire = random.choice(self.objets_disponibles)
                objet_nom = self.noms_objets[objet_aleatoire]
                if objet_aleatoire == 'O3':
                    # Générer des coordonnées aléatoires pour la position du verglas
                    V1 = random.randint(1, 5)
                    V2 = random.randint(1, 5)
                    # Assurez-vous que les coordonnées sont valides
                    if self.position_valide(V1, V2):
                        # Modifier la case correspondante en verglas
                        self.map[V1][V2] = 'V'
                        # Afficher visuellement le changement sur le plateau
                        self.afficher_plateau()
                        # Supprimer le cadeau mystère de la carte et de la liste des cadeaux mystères
                        self.canvas_plateau.delete(f"cadeau_{ligne}_{colonne}")
                        self.cadeaux_mysteres.remove((ligne, colonne))
                        # Replacer les cadeaux mystères sur la carte
                        self.placer_cadeaux()
                        # Mettre à jour la variable indiquant la présence d'un cadeau mystère
                        self.cadeau_present = False
                    else:
                        messagebox.showwarning("Coordonnées invalides", "Les coordonnées générées pour le verglas sont invalides.")
                # Autres actions à effectuer pour d'autres types d'objets (non inclus dans cet exemple)
                # ...
                #self.canvas_plateau.delete(f"cadeau_{ligne}_{colonne}")
                self.cadeaux_mysteres.remove((ligne, colonne))  # Supprimer la position du cadeau mystère de la liste
                self.placer_cadeaux()  # Replacer les cadeaux mystères sur la carte
                self.cadeau_present = False  # Mettre à jour la variable indiquant la présence d'un cadeau mystère
        else:
            messagebox.showinfo("Cadeau déjà utilisé", "Ce cadeau mystère a déjà été utilisé.")


    def mettre_a_jour_jeu(self):
        self.compteur_cadeaux += 1

            # Si le compteur atteint le délai de réapparition et qu'aucun cadeau n'est présent...
        if self.compteur_cadeaux >= self.delai_reapparition and not self.cadeau_present:
            # Placer un nouveau cadeau mystère
            self.placer_cadeaux()
            # Réinitialiser le compteur
            self.compteur_cadeaux = 0
            # Mettre à jour la variable indiquant la présence d'un cadeau
            self.cadeau_present = True
        else:
            # Si le cadeau réapparaît, définir cadeau_present à False
            self.cadeau_present = False




    def activer_controles(self):
        pass

    def position_valide(self, ligne, colonne):
        if 0 <= ligne < len(self.map) and 0 <= colonne < len(self.map[0]):
            case = self.map[ligne][colonne]
            return case in ['X', 'V', 'T', 'B', 'H', 'F', 'D']
        return False

    def passer_au_joueur_suivant(self):
        if not self.joueurs:
            messagebox.showinfo("Fin du jeu", "Tous les joueurs sont morts. Le jeu est terminé.")
            self.quit() # Arrêter le jeu
            return

        joueur_actif = self.joueurs[self.tour]

        # Si le joueur est mort, passer au joueur suivant
        while joueur_actif is None:
            self.tour += 1
            joueur_actif = self.joueurs[self.tour]

        ligne, colonne = joueur_actif[1], joueur_actif[2]

        while not self.position_valide(ligne, colonne):
            self.tour += 1
            joueur_actif = self.joueurs[self.tour]
        # Si tous les joueurs sont morts, quitter la boucle
        if not self.joueurs:
            return
        if self.tour % 5 == 0:
            self.initialiser_cadeaux_mysteres()
        # Si tous les joueurs sont morts, sauf le joueur P1, déclarer la victoire du joueur P1
        if len(self.joueurs) == 1 and self.joueurs[0][0] == 1:
            messagebox.showinfo("Fin du jeu", "Le joueur P1 a gagné !")
            self.quit()
            return

        self.objet_ramasse = False
        index_joueur_actuel = self.joueur_actif - 1
        index_joueur_suivant = (index_joueur_actuel + 1) % len(self.joueurs)

        # Tant que le joueur suivant est mort et qu'il reste plus d'un joueur, continuer à chercher
        while self.joueurs[index_joueur_suivant] is None and len(self.joueurs) > 1:
            index_joueur_suivant = (index_joueur_suivant + 1) % len(self.joueurs)

        # Si tous les joueurs sont morts, quitter la boucle
        if not self.joueurs:
            return

        self.joueur_actif = index_joueur_suivant + 1
        messagebox.showinfo("Tour suivant", f"Joueur {self.joueur_actif}, c'est à vous de jouer !")

        if index_joueur_suivant == 0 and not self.cadeau_present:
            self.placer_cadeaux() # Placer un nouveau cadeau mystère
            self.cadeau_present = True # Mettre à jour la variable indiquant la présence d'un cadeau mystère



app = MenuGraphique()
app.mainloop()



