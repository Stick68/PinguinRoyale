## Protocole d'utilisation

### Windows:
#### Installation:
	- Installer python 3 ou supérieur 
	([ici](https://www.python.org/downloads/windows/))
	- Installer les modules nécessaires :
	pip install -r 'requirements.txt'
#### Exécution:
	- Depuis le repertoire 'Sources': 'jeu sans graphismes.py' pour la version fonctionnelle et 'Jeu_initial.py' pour tester avec quelques images.
		